---
aliases: e, devtools
---

To better help you, we need your error logs!

To collect them, please
- open DevTools with `ctrl + shift + i` / `cmd + option + i`
- on the top right, click where it says "Default levels" and uncheck everything but "Errors"
- screenshot all errors and post them
